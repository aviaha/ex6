import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from "@angular/router";
import {FormControl, Validators} from '@angular/forms';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  email:string;
  password:string;
  name:string;
  code='';
  message='';
  wrong = "";
  result: boolean;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  signup(){
    // console.log("sign up clicked" +' '+ this.email+' '+this.password+' '+this.name)
    this.wrong = "";
    this.result = /^[a-z0-9]+$/i.test(this.password);
   if (this.result) {
      this.wrong = "Password must contain a symbol ($,#, etc.)";
      return
    }
    this.authService.signup(this.email,this.password)//כל זה מחזיר promise
    .then( value =>{
      this.authService.updateProfile(value.user,this.name);
    }).then(value =>{
      this.router.navigate(['/welcome'])//אחרי שעושים הרשמה זה עובר לדף הראשי טודוס
    }).catch(err =>{
      this.code=err.code;
      this.message=err.message;
      console.log(err);
    }) 
   }


  constructor(private authService:AuthService,private router:Router, public afAuth: AngularFireAuth) { }

  ngOnInit() {
  }
  onLoginGoogle()
  {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    this.router.navigate(['/welcome']);
  }

  onLoginFacebook()
  {
    this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
    this.router.navigate(['/welcome']);
  }

}
